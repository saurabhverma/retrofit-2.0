package com.saurabh.retrofit2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.saurabh.retrofit2.retrofit.APIError;
import com.saurabh.retrofit2.retrofit.ResponseResolver;
import com.saurabh.retrofit2.retrofit.ServiceGenerator;
import com.saurabh.retrofit2.retrofit.WebServices;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;

/**
 * Developer: Saurabh Verma
 * Dated: 6/13/16.
 */
public class MainActivity extends AppCompatActivity {

    private TextView tvResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvResponse = (TextView) findViewById(R.id.tvResponse);


        // Create a very simple REST adapter which points the API endpoint.
/*        WebServices mWebServices = ServiceGenerator.createService(WebServices.class);

        Call<List<User>> call = mWebServices.getUser();
        call.enqueue(new ResponseResolver<List<User>>(MainActivity.this) {
            @Override
            public void success(List<User> mUserList) {
                String text = "";
                for (int i = 0; i < mUserList.size(); i++)
                    text = text + "\n" + (i + 1) + " ) " + mUserList.get(i).getName();
                tvResponse.setText(text);
            }

            @Override
            public void failure(APIError error) {
                Toast.makeText(MainActivity.this, error.getStatusCode() + " : " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

*//**
 * Google API call for get directions
 * @param origin latlng
 * @param destination latlng
 * @param senser
 * @param unit
 * @param mode
 * *//*
        WebServices mGoogleWEbService = ServiceGenerator.createGooglerService(WebServices.class);
        Call<Object> mObjectCall = mGoogleWEbService.getDriections("30.7333, 76.7794", "30.753, 76.7794", "", "", "driving");
        mObjectCall.enqueue(new ResponseResolver<Object>(MainActivity.this) {
            @Override
            public void success(Object obj) {

            }

            @Override
            public void failure(APIError error) {
                Toast.makeText(MainActivity.this, error.getStatusCode() + " : " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });*/
        /**
         * Put default image in file when user not selected any image
         * */
        File file = new File(getCacheDir() + "/placeholder.png");
        if (!file.exists()) try {
            InputStream is = getAssets().open("placeholder.png");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(buffer);
            fos.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }


        WebServices mWebServices = ServiceGenerator.createService(WebServices.class);



        // create RequestBody instance from file
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body = MultipartBody.Part.createFormData("picture", file.getName(), requestFile);

        // add another part within the multipart request
        String descriptionString = "hello, this is description speaking";
        RequestBody description = RequestBody.create(MediaType.parse("multipart/form-data"), descriptionString);
        RequestBody firtName = RequestBody.create(MediaType.parse("multipart/form-data"), "testfirst");
        RequestBody lastName = RequestBody.create(MediaType.parse("multipart/form-data"), "testLast");
        RequestBody callingName = RequestBody.create(MediaType.parse("multipart/form-data"), "callingName");
        RequestBody phoneNumber = RequestBody.create(MediaType.parse("multipart/form-data"), "12345927861");
        RequestBody gender = RequestBody.create(MediaType.parse("multipart/form-data"), "MALE");
        RequestBody password = RequestBody.create(MediaType.parse("multipart/form-data"), "123456");
        RequestBody deviceToken = RequestBody.create(MediaType.parse("multipart/form-data"), "gdgdg");
        RequestBody appVersion = RequestBody.create(MediaType.parse("multipart/form-data"), "100");
        RequestBody city = RequestBody.create(MediaType.parse("multipart/form-data"), "ANDROID");
        RequestBody zipcode = RequestBody.create(MediaType.parse("multipart/form-data"), "APP");

        // finally, execute the request
        Call<Object> call = mWebServices.updateProfileWithImage(requestFile,firtName,lastName,callingName,phoneNumber,gender,password,deviceToken,appVersion,city,zipcode);
        call.enqueue(new ResponseResolver<Object>(MainActivity.this) {
            @Override
            public void success(Object o) {
            }

            @Override
            public void failure(APIError error) {
                Toast.makeText(MainActivity.this, error.getStatusCode() + " : " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}
