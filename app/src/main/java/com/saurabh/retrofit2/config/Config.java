package com.saurabh.retrofit2.config;

/**
 * Developer: Saurabh Verma
 * Dated: 6/13/16.
 */
public class Config {
    static String GCM_PROJECT_NUMBER = "";
    static String BASE_URL = "";
    static AppMode appMode = AppMode.DEV;
    static String GOOGLE_URL = "";

    static public String getBaseURL() {
        init(appMode);
        return BASE_URL;
    }

    static public String getGoogleUrl() {
        init(appMode);
        return GOOGLE_URL;
    }


    static public String getGCMProjectNumber() {
        init(appMode);
        return GCM_PROJECT_NUMBER;
    }


    /**
     * Initialize all the variable in this method
     *
     * @param appMode
     */
    public static void init(AppMode appMode) {

        switch (appMode) {
            case DEV:
                //BASE_URL = "http://jsonplaceholder.typicode.com/";
                BASE_URL = "http://54.186.23.246:8000/";
                GCM_PROJECT_NUMBER = "937760270743";
                GOOGLE_URL = "http://maps.googleapis.com/";
                break;

            case TEST:
                BASE_URL = "http://jsonplaceholder.typicode.com/";
                GCM_PROJECT_NUMBER = "937760270743";
                GOOGLE_URL = "http://maps.googleapis.com/";
                break;

            case LIVE:
                BASE_URL = "http://jsonplaceholder.typicode.com/";
                GCM_PROJECT_NUMBER = "937760270743";
                GOOGLE_URL = "http://maps.googleapis.com/";
                break;
        }

    }

    public enum AppMode {
        DEV, TEST, LIVE
    }
}
