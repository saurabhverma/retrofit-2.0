package com.saurabh.retrofit2.retrofit;

/**
 * Developer: Saurabh Verma
 * Dated: 6/13/16.
 */
public class APIError {

    private int statusCode;
    private int code;
    private String message;
    private String error;
    private String error_message;
    private String status;
    private Object data;


    public APIError(int statusCode, String message) {
        this.message = message;
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        if (statusCode == 0)
            if (code != 0)
                return code;
            else
                statusCode = 900;
        return statusCode;
    }

    public String getMessage() {
        if (message == null) {
            if (error != null)
                return error;
            else if (error_message != null)
                return error_message;
            else
                return "Some error occurred.";
        } else
            return message;
    }

    public boolean isEmptyObject() {
        if (statusCode == 0 && code == 0 && message == null && error == null && error_message == null && status == null && data == null)
            return true;
        else
            return false;
    }
}
