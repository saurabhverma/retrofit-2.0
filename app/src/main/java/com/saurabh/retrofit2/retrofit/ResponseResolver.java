package com.saurabh.retrofit2.retrofit;

import android.content.Context;

import org.apache.http.conn.ConnectTimeoutException;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Developer: Saurabh Verma
 * Dated: 6/13/16.
 */
public abstract class ResponseResolver<T> implements Callback<T> {
    private Context mContext;
    private final String NO_INTERNET_MESSAGE = "PLEASE CHECK INTERNET CONNECTIVITY STATUS";
    private final String REMOTE_SERVER_FAILED_MESSAGE = "REMOTE SERVER FAILED TO RESPOND";
    private final String CONNECTION_TIME_OUT_MESSAGE = "CONNECTION TIMED OUT";
    private final String UNEXPECTED_ERROR_OCCURRED = "INTERNET CONNECTIVITY ISSUE";

    public ResponseResolver(Context mContext) {
        this.mContext = mContext;
    }

    public abstract void success(T t);

    public abstract void failure(APIError error);

    @Override
    public void onResponse(Call<T> t, Response<T> tResponse) {
        if (tResponse.isSuccessful())
            success(tResponse.body());
        else
            failure(ErrorUtils.parseError(tResponse));
    }

    @Override
    public void onFailure(Call<T> t, Throwable throwable) {
        failure(new APIError(900, resolveNetworkError(throwable)));
    }

    /**
     * Method resolve network errors
     *
     * @param cause
     * @return
     */
    private String resolveNetworkError(Throwable cause) {
        if (cause instanceof UnknownHostException)
            return NO_INTERNET_MESSAGE;
        if (cause instanceof SocketTimeoutException)
            return REMOTE_SERVER_FAILED_MESSAGE;
        else if (cause instanceof ConnectTimeoutException)
            return CONNECTION_TIME_OUT_MESSAGE;
        return UNEXPECTED_ERROR_OCCURRED;
    }
}
