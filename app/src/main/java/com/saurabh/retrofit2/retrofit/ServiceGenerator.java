package com.saurabh.retrofit2.retrofit;

import com.saurabh.retrofit2.config.Config;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Developer: Saurabh Verma
 * Dated: 6/13/16.
 */
public class ServiceGenerator {
    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();


    private static Retrofit.Builder builder = new Retrofit.Builder()
            .baseUrl(Config.getBaseURL())
            .addConverterFactory(GsonConverterFactory.create());

    public static <S> S createService(Class<S> serviceClass) {
        return retrofit().create(serviceClass);
    }

    public static <S> S createGooglerService(Class<S> serviceClass) {
        return googleRetrofit().create(serviceClass);
    }

    public static Retrofit retrofit() {
        /**
         * Set log level in retrofit 2.0 by using a separate logging interceptor
         * */
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        // add your other interceptors
        // add logging as last interceptor
        httpClient.addInterceptor(logging);  // <-- this is the important line!


        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(Config.getBaseURL())
                .addConverterFactory(GsonConverterFactory.create());
        return builder.client(httpClient.build()).build();
    }

    public static Retrofit googleRetrofit() {
        /**
         * Set log level in retrofit 2.0 by using a separate logging interceptor
         * */
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        // add your other interceptors
        // add logging as last interceptor
        httpClient.addInterceptor(logging);  // <-- this is the important line!

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(Config.getGoogleUrl())
                .addConverterFactory(GsonConverterFactory.create());
        return builder.client(httpClient.build()).build();
    }

}
