package com.saurabh.retrofit2.retrofit;

import com.saurabh.retrofit2.model.User;

import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Developer: Saurabh Verma
 * Dated: 6/13/16.
 */
public interface WebServices {
    @GET("users")
    Call<List<User>> getUser();

    @GET("maps/api/directions/json")
    Call<Object> getDriections(@Query("origin") String origin,
                               @Query("destination") String destination,
                               @Query("sensor") String sensor,
                               @Query("units") String units,
                               @Query("mode") String mode);

    @Multipart
    @POST("/api/Nurses/registerNurses")
    Call<Object> updateProfileWithImage(@Part("profilePic") RequestBody description,
                                        @Part("firstName") RequestBody firstName,
                                        @Part("lastName") RequestBody lastName,
                                        @Part("callingName") RequestBody callingName,
                                        @Part("phoneNumber") RequestBody phoneNumber,
                                        @Part("gender") RequestBody gender,
                                        @Part("password") RequestBody password,
                                        @Part("deviceToken") RequestBody deviceToken,
                                        @Part("appVersion") RequestBody appVersion,
                                        @Part("deviceType") RequestBody deviceType,
                                        @Part("Logintype") RequestBody Logintype);


}
