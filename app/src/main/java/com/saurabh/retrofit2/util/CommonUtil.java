package com.saurabh.retrofit2.util;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.text.Html;

import com.saurabh.retrofit2.R;

/**
 * Developer: Saurabh Verma
 * Dated: 6/13/16.
 */
public class CommonUtil {
    private static AlertDialog.Builder alertDialog;

    /**
     * Method to show single button default dialog
     */
    public static void showDialog(Context context, String msg,String title) {
        if (alertDialog == null) {
            alertDialog = new AlertDialog.Builder(context);
            alertDialog.setCancelable(false);
            alertDialog.setTitle(title);
            alertDialog.setMessage(Html.fromHtml(msg));
            alertDialog.setPositiveButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    alertDialog = null;
                }
            });
            alertDialog.show();
        }
    }
}
